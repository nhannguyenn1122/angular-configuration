# <Angular-base-configuration>

## Description

- This project focuses on development process
- Self-studying how to create a base project
- Seperate code into some checking-commit-format processes

## Content

-[ENV-config](#Env) -[Prettier](#Prettier) -[Eslint](#Eslint) -[Lint-staged](#Lint-staged) -[Husky](#Husky)

## Env

![URL]:(https://balramchavan.medium.com/separating-production-and-development-http-urls-using-environment-ts-file-in-angular-4c2dd0c5a8b0);

![URL]:(https://balramchavan.medium.com/configure-and-build-angular-application-for-different-environments-7e94a3c0af23);

## Prettier

(Prettier, eslint-plugin-prettier)

![prettier]:(https://prettier.io/);

- Format code when coding

![eslint-plugin-prettier]:(https://github.com/prettier/eslint-plugin-prettier);

- Fixing conflict between prettier and eslint

## Eslint

(Angular-eslint, eslint)

![URL]:(https://eslint.org/);

- Rule when coding

## Lint-staged

![URL]:(https://www.npmjs.com/package/lint-staged);
![URL]:(https://github.com/elunic/node-ng-lint-staged#readme);

- Control commit Step

## Husky

![URL]:https://typicode.github.io/husky);

- Control hooks commit
