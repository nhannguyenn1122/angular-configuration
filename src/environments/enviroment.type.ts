export interface EnvInterface {
  production: boolean;
  baseUrl: string;
}
